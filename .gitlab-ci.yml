stages:
    - scanning
    - defectdojo_create_engagement
    - defectdojo_publish
    - zap_proxy_scan
    - zap_proxy_publish

sonarqube-scan:
  stage: scanning
  image:
      name: sonarsource/sonar-scanner-cli:latest
      entrypoint: [""]
  script:
      - sonar-scanner
  allow_failure: true

owasp_dependency_check:
  image:
      name: registry.gitlab.com/gitlab-ci-utils/docker-dependency-check:latest
      entrypoint: [""]
  stage: scanning
  script:
      # Job will scan the project root folder and fail if any vulnerabilities with CVSS > 0 are found
      - /usr/share/dependency-check/bin/dependency-check.sh --scan "./" --format ALL --project "$CI_PROJECT_NAME" --failOnCVSS 0
      # Dependency Check will only fail the job based on CVSS scores, and in some cases vulnerabilities do not
      # have CVSS scores (e.g. those from NPM audit), so they don't cause failure.  To fail for any vulnerabilities
      # grep the resulting report for any "vulnerabilities" sections and exit if any are found (count > 0).
      - if [ $(grep -c "vulnerabilities" dependency-check-report.json) -gt 0 ]; then exit 2; fi
  allow_failure: true
  artifacts:
      when: always
      paths:
         # Save the HTML and JSON report artifacts
         - "./dependency-check-report.html"
         - "./dependency-check-report.json"
         - "dependency-check-report.xml"

#-------------------------- Defect Dojo ------------------------------------
variables:
  DEFECTDOJO_ENGAGEMENT_PERIOD: 28
  DEFECTDOJO_ENGAGEMENT_STATUS: "Not Started"
  DEFECTDOJO_ENGAGEMENT_BUILD_SERVER: "null"
  DEFECTDOJO_ENGAGEMENT_SOURCE_CODE_MANAGEMENT_SERVER: "null"
  DEFECTDOJO_ENGAGEMENT_ORCHESTRATION_ENGINE: "null"
  DEFECTDOJO_ENGAGEMENT_DEDUPLICATION_ON_ENGAGEMENT: "false"
  DEFECTDOJO_ENGAGEMENT_THREAT_MODEL: "true"
  DEFECTDOJO_ENGAGEMENT_API_TEST: "true"
  DEFECTDOJO_ENGAGEMENT_PEN_TEST: "true"
  DEFECTDOJO_ENGAGEMENT_CHECK_LIST: "true"
  DEFECTDOJO_NOT_ON_MASTER: "false"


defectdojo_create_engagement_sonarqube:
  stage: defectdojo_create_engagement
  image: alpine
  variables:
    GIT_STRATEGY: none
  before_script:
    - apk add curl jq coreutils
    - TODAY=`date +%Y-%m-%d`
    - ENDDAY=$(date -d "+${DEFECTDOJO_ENGAGEMENT_PERIOD} days" +%Y-%m-%d)
  script:
    - |
        ENGAGEMENTID=`curl --fail --location --request POST "${DEFECTDOJO_URL}/engagements/" \
              --header "Authorization: Token ${DEFECTDOJO_TOKEN}" \
              --header 'Content-Type: application/json' \
                --data-raw "{
                  \"tags\": [\"GITLAB-CI\"],
                  \"name\": \"SonarqubeDev_#${CI_PIPELINE_ID}\",
                  \"description\": \"${CI_COMMIT_DESCRIPTION}\",
                  \"version\": \"${CI_COMMIT_REF_NAME}\",
                  \"first_contacted\": \"${TODAY}\",
                  \"target_start\": \"${TODAY}\",
                  \"target_end\": \"${ENDDAY}\",
                  \"reason\": \"string\",
                  \"tracker\": \"${CI_PROJECT_URL}/-/issues\",
                  \"threat_model\": \"${DEFECTDOJO_ENGAGEMENT_THREAT_MODEL}\",
                  \"api_test\": \"${DEFECTDOJO_ENGAGEMENT_API_TEST}\",
                  \"pen_test\": \"${DEFECTDOJO_ENGAGEMENT_PEN_TEST}\",
                  \"check_list\": \"${DEFECTDOJO_ENGAGEMENT_CHECK_LIST}\",
                  \"status\": \"${DEFECTDOJO_ENGAGEMENT_STATUS}\",
                  \"engagement_type\": \"CI/CD\",
                  \"build_id\": \"SonarqubeDev_${CI_PIPELINE_ID}\",
                  \"commit_hash\": \"${CI_COMMIT_SHORT_SHA}\",
                  \"branch_tag\": \"${CI_COMMIT_REF_NAME}\",
                  \"deduplication_on_engagement\": \"${DEFECTDOJO_ENGAGEMENT_DEDUPLICATION_ON_ENGAGEMENT}\",
                  \"product\": \"${DEFECTDOJO_PRODUCTID}\",
                  \"source_code_management_uri\": \"${CI_PROJECT_URL}\",
                  \"build_server\": ${DEFECTDOJO_ENGAGEMENT_BUILD_SERVER},
                  \"source_code_management_server\": ${DEFECTDOJO_ENGAGEMENT_SOURCE_CODE_MANAGEMENT_SERVER},
                  \"orchestration_engine\": ${DEFECTDOJO_ENGAGEMENT_ORCHESTRATION_ENGINE}
                }" | jq -r '.id'`
    - echo "DEFECTDOJO_ENGAGEMENTID=${ENGAGEMENTID}" >> defectdojo.env
  artifacts:
    reports:
      dotenv: defectdojo.env


defectdojo_publish_gitlab:
  stage: defectdojo_publish
  image: alpine
  allow_failure: true
  variables:
    DEFECTDOJO_SCAN_MINIMUM_SEVERITY: "Info"
    DEFECTDOJO_SCAN_ACTIVE: "true"
    DEFECTDOJO_SCAN_VERIFIED: "false"
    DEFECTDOJO_SCAN_CLOSE_OLD_FINDINGS: "true"
    DEFECTDOJO_SCAN_PUSH_TO_JIRA: "false"
    DEFECTDOJO_SCAN_ENVIRONMENT: "Default"
    DEFECTDOJO_SCAN_TEST_TYPE: "SonarQube API Import"
    DEFECTDOJO_PRODUCT_TYPE: "gitlab-community"
    DEFECTDOJO_PRODUCT_NAME: "gitlab-defectdojo"

    
  before_script:
    - apk add curl coreutils
    - TODAY=`date +%Y-%m-%d`
  script:
    - |
        curl --request POST "${DEFECTDOJO_URL}/import-scan/" \
            --header "Authorization: Token ${DEFECTDOJO_TOKEN}" \
            --form "scan_date=\"${TODAY}\"" \
            --form "minimum_severity=\"${DEFECTDOJO_SCAN_MINIMUM_SEVERITY}\"" \
            --form "active=\"${DEFECTDOJO_SCAN_ACTIVE}\"" \
            --form "verified=\"${DEFECTDOJO_SCAN_VERIFIED}\"" \
            --form "scan_type=\"${DEFECTDOJO_SCAN_TYPE}\"" \
            --form "engagement_name=\"SonarqubeDev_#${CI_PIPELINE_ID}\"" \
            --form "close_old_findings=\"${DEFECTDOJO_SCAN_CLOSE_OLD_FINDINGS}\"" \
            --form "product_name=\"${DEFECTDOJO_PRODUCT_NAME}\"" \
            --form "scan_type=\"${DEFECTDOJO_SCAN_TEST_TYPE}\"" \
            --form "environment=\"${DEFECTDOJO_SCAN_ENVIRONMENT}\""

#------------- Dependency check ---------------------#


defectdojo_create_engagement_dep_check:
  stage: defectdojo_create_engagement
  image: alpine
  variables:
    GIT_STRATEGY: none
  before_script:
    - apk add curl jq coreutils
    - TODAY=`date +%Y-%m-%d`
    - ENDDAY=$(date -d "+${DEFECTDOJO_ENGAGEMENT_PERIOD} days" +%Y-%m-%d)
  script:
    - |
        ENGAGEMENTID=`curl --fail --location --request POST "${DEFECTDOJO_URL}/engagements/" \
              --header "Authorization: Token ${DEFECTDOJO_TOKEN}" \
              --header 'Content-Type: application/json' \
                --data-raw "{
                  \"tags\": [\"GITLAB-CI\"],
                  \"name\": \"DependencyCheck_#${CI_PIPELINE_ID}\",
                  \"description\": \"${CI_COMMIT_DESCRIPTION}\",
                  \"version\": \"${CI_COMMIT_REF_NAME}\",
                  \"first_contacted\": \"${TODAY}\",
                  \"target_start\": \"${TODAY}\",
                  \"target_end\": \"${ENDDAY}\",
                  \"reason\": \"string\",
                  \"tracker\": \"${CI_PROJECT_URL}/-/issues\",
                  \"threat_model\": \"${DEFECTDOJO_ENGAGEMENT_THREAT_MODEL}\",
                  \"api_test\": \"${DEFECTDOJO_ENGAGEMENT_API_TEST}\",
                  \"pen_test\": \"${DEFECTDOJO_ENGAGEMENT_PEN_TEST}\",
                  \"check_list\": \"${DEFECTDOJO_ENGAGEMENT_CHECK_LIST}\",
                  \"status\": \"${DEFECTDOJO_ENGAGEMENT_STATUS}\",
                  \"engagement_type\": \"CI/CD\",
                  \"build_id\": \"DependencyCheck_${CI_PIPELINE_ID}\",
                  \"commit_hash\": \"${CI_COMMIT_SHORT_SHA}\",
                  \"branch_tag\": \"${CI_COMMIT_REF_NAME}\",
                  \"deduplication_on_engagement\": \"${DEFECTDOJO_ENGAGEMENT_DEDUPLICATION_ON_ENGAGEMENT}\",
                  \"product\": \"${DEFECTDOJO_PRODUCTID}\",
                  \"source_code_management_uri\": \"${CI_PROJECT_URL}\",
                  \"build_server\": ${DEFECTDOJO_ENGAGEMENT_BUILD_SERVER},
                  \"source_code_management_server\": ${DEFECTDOJO_ENGAGEMENT_SOURCE_CODE_MANAGEMENT_SERVER},
                  \"orchestration_engine\": ${DEFECTDOJO_ENGAGEMENT_ORCHESTRATION_ENGINE}
                }" | jq -r '.id'`
    - echo "DEFECTDOJO_ENGAGEMENTID=${ENGAGEMENTID}" >> defectdojo_dep.env
  artifacts:
    reports:
      dotenv: defectdojo_dep.env


defectdojo_publish_gitlab_dep_check:
  stage: defectdojo_publish
  image: alpine
  allow_failure: true
  variables:
    DEFECTDOJO_SCAN_MINIMUM_SEVERITY: "Info"
    DEFECTDOJO_SCAN_ACTIVE: "true"
    DEFECTDOJO_SCAN_VERIFIED: "false"
    DEFECTDOJO_SCAN_CLOSE_OLD_FINDINGS: "true"
    DEFECTDOJO_SCAN_PUSH_TO_JIRA: "false"
    DEFECTDOJO_SCAN_ENVIRONMENT: "Default"
    DEFECTDOJO_SCAN_TEST_TYPE: "Dependency Check Scan"
    DEFECTDOJO_PRODUCT_TYPE: "gitlab-community"
    DEFECTDOJO_PRODUCT_NAME: "gitlab-defectdojo"
    
  before_script:
    - apk add curl coreutils
    - TODAY=`date +%Y-%m-%d`
  script:
    - |
        curl -X POST "${DEFECTDOJO_URL}/import-scan/" -H  "accept: application/json" -H  "Content-Type: multipart/form-data" -H  "X-CSRFToken: ujMSwx6OazuMFYFeovC7SFWNZ4MeCRS7dqRYTlAHYXBDCGvw5aoC2iWR9m5UCwts" -H "Authorization: Token 0dfc2ae06f011369f4eefbcf3e067efb9627ce09" -F "scan_date=${TODAY}" -F "minimum_severity=Info" -F "active=true" -F "verified=false" -F "scan_type=Dependency Check Scan" -F "product_name=gitlab-defectdojo" -F "file=@dependency-check-report.xml;type=text/xml" -F "engagement_name=DependencyCheck_#${CI_PIPELINE_ID}" -F "close_old_findings=true" -F "push_to_jira=false"


#------------- Trivy ---------------------#

trivy:
  stage: scanning
  image: docker:stable-git
  before_script:
    - docker build -t trivy-ci-test:${CI_COMMIT_REF_NAME} .
    - apk add curl jq coreutils
    - export VERSION=$(curl --silent "https://api.github.com/repos/aquasecurity/trivy/releases/latest" | grep '"tag_name":' | sed -E 's/.*"v([^"]+)".*/\1/')
    - wget https://github.com/aquasecurity/trivy/releases/download/v${VERSION}/trivy_${VERSION}_Linux-64bit.tar.gz
    - tar zxvf trivy_${VERSION}_Linux-64bit.tar.gz
  variables:
    DOCKER_DRIVER: overlay2
  allow_failure: true
  services:
    - docker:stable-dind
  script:
    - ./trivy image -f json -o results.json trivy-ci-test:${CI_COMMIT_REF_NAME}
  cache:
    key: trivy-cache-$CI_COMMIT_REF_SLUG
    paths:
      - .cache/trivy
  artifacts:
      paths:
         # Save JSON report artifacts
         - "results.json"


defectdojo_create_engagement_trivy:
  stage: defectdojo_create_engagement
  image: alpine
  variables:
    GIT_STRATEGY: none
  before_script:
    - apk add curl jq coreutils
    - TODAY=`date +%Y-%m-%d`
    - ENDDAY=$(date -d "+${DEFECTDOJO_ENGAGEMENT_PERIOD} days" +%Y-%m-%d)
  script:
    - |
        ENGAGEMENTID=`curl --fail --location --request POST "${DEFECTDOJO_URL}/engagements/" \
              --header "Authorization: Token ${DEFECTDOJO_TOKEN}" \
              --header 'Content-Type: application/json' \
                --data-raw "{
                  \"tags\": [\"GITLAB-CI\"],
                  \"name\": \"Trivy_#${CI_PIPELINE_ID}\",
                  \"description\": \"${CI_COMMIT_DESCRIPTION}\",
                  \"version\": \"${CI_COMMIT_REF_NAME}\",
                  \"first_contacted\": \"${TODAY}\",
                  \"target_start\": \"${TODAY}\",
                  \"target_end\": \"${ENDDAY}\",
                  \"reason\": \"string\",
                  \"tracker\": \"${CI_PROJECT_URL}/-/issues\",
                  \"threat_model\": \"${DEFECTDOJO_ENGAGEMENT_THREAT_MODEL}\",
                  \"api_test\": \"${DEFECTDOJO_ENGAGEMENT_API_TEST}\",
                  \"pen_test\": \"${DEFECTDOJO_ENGAGEMENT_PEN_TEST}\",
                  \"check_list\": \"${DEFECTDOJO_ENGAGEMENT_CHECK_LIST}\",
                  \"status\": \"${DEFECTDOJO_ENGAGEMENT_STATUS}\",
                  \"engagement_type\": \"CI/CD\",
                  \"build_id\": \"Trivy_${CI_PIPELINE_ID}\",
                  \"commit_hash\": \"${CI_COMMIT_SHORT_SHA}\",
                  \"branch_tag\": \"${CI_COMMIT_REF_NAME}\",
                  \"deduplication_on_engagement\": \"${DEFECTDOJO_ENGAGEMENT_DEDUPLICATION_ON_ENGAGEMENT}\",
                  \"product\": \"${DEFECTDOJO_PRODUCTID}\",
                  \"source_code_management_uri\": \"${CI_PROJECT_URL}\",
                  \"build_server\": ${DEFECTDOJO_ENGAGEMENT_BUILD_SERVER},
                  \"source_code_management_server\": ${DEFECTDOJO_ENGAGEMENT_SOURCE_CODE_MANAGEMENT_SERVER},
                  \"orchestration_engine\": ${DEFECTDOJO_ENGAGEMENT_ORCHESTRATION_ENGINE}
                }" | jq -r '.id'`
    - echo "DEFECTDOJO_ENGAGEMENTID=${ENGAGEMENTID}" >> defectdojo_trivy.env
  artifacts:
    reports:
      dotenv: defectdojo_trivy.env


defectdojo_publish_gitlab_trivy:
  stage: defectdojo_publish
  image: alpine
  allow_failure: true
  variables:
    DEFECTDOJO_SCAN_MINIMUM_SEVERITY: "Info"
    DEFECTDOJO_SCAN_ACTIVE: "true"
    DEFECTDOJO_SCAN_VERIFIED: "false"
    DEFECTDOJO_SCAN_CLOSE_OLD_FINDINGS: "true"
    DEFECTDOJO_SCAN_PUSH_TO_JIRA: "false"
    DEFECTDOJO_SCAN_ENVIRONMENT: "Default"
    DEFECTDOJO_SCAN_TEST_TYPE: "Trivy Scan"
    DEFECTDOJO_PRODUCT_TYPE: "gitlab-community"
    DEFECTDOJO_PRODUCT_NAME: "gitlab-defectdojo"
    
  before_script:
    - apk add curl coreutils
    - TODAY=`date +%Y-%m-%d`
  script:
    - |
        curl -X POST "${DEFECTDOJO_URL}/import-scan/" -H  "accept: application/json" -H  "Content-Type: multipart/form-data" -H  "X-CSRFToken: ujMSwx6OazuMFYFeovC7SFWNZ4MeCRS7dqRYTlAHYXBDCGvw5aoC2iWR9m5UCwts" -H "Authorization: Token 0dfc2ae06f011369f4eefbcf3e067efb9627ce09" -F "scan_date=${TODAY}" -F "minimum_severity=Info" -F "active=true" -F "verified=false" -F "scan_type=Trivy Scan" -F "product_name=gitlab-defectdojo" -F "file=@results.json;type=text/json" -F "engagement_name=Trivy_#${CI_PIPELINE_ID}" -F "close_old_findings=true" -F "push_to_jira=false"

#----------------------------------  DAST ZAP ------------------------------

dast:
  stage: zap_proxy_scan
  image: owasp/zap2docker-stable
  variables:
    website: "https://www.admingb.com"
  script:
    - mkdir /zap/wrk
    - /zap/zap-baseline.py -t $website -x report_zap.xml || true
    - cp /zap/wrk/report_zap.xml .
  allow_failure: true  
  artifacts:
    paths: [report_zap.xml]



defectdojo_create_engagement_zap_proxy:
  stage: defectdojo_create_engagement
  image: alpine
  variables:
    GIT_STRATEGY: none
  before_script:
    - apk add curl jq coreutils
    - TODAY=`date +%Y-%m-%d`
    - ENDDAY=$(date -d "+${DEFECTDOJO_ENGAGEMENT_PERIOD} days" +%Y-%m-%d)
  script:
    - |
        ENGAGEMENTID=`curl --fail --location --request POST "${DEFECTDOJO_URL}/engagements/" \
              --header "Authorization: Token ${DEFECTDOJO_TOKEN}" \
              --header 'Content-Type: application/json' \
                --data-raw "{
                  \"tags\": [\"GITLAB-CI\"],
                  \"name\": \"Zap_proxy_#${CI_PIPELINE_ID}\",
                  \"description\": \"${CI_COMMIT_DESCRIPTION}\",
                  \"version\": \"${CI_COMMIT_REF_NAME}\",
                  \"first_contacted\": \"${TODAY}\",
                  \"target_start\": \"${TODAY}\",
                  \"target_end\": \"${ENDDAY}\",
                  \"reason\": \"string\",
                  \"tracker\": \"${CI_PROJECT_URL}/-/issues\",
                  \"threat_model\": \"${DEFECTDOJO_ENGAGEMENT_THREAT_MODEL}\",
                  \"api_test\": \"${DEFECTDOJO_ENGAGEMENT_API_TEST}\",
                  \"pen_test\": \"${DEFECTDOJO_ENGAGEMENT_PEN_TEST}\",
                  \"check_list\": \"${DEFECTDOJO_ENGAGEMENT_CHECK_LIST}\",
                  \"status\": \"${DEFECTDOJO_ENGAGEMENT_STATUS}\",
                  \"engagement_type\": \"CI/CD\",
                  \"build_id\": \"Zap_proxy_${CI_PIPELINE_ID}\",
                  \"commit_hash\": \"${CI_COMMIT_SHORT_SHA}\",
                  \"branch_tag\": \"${CI_COMMIT_REF_NAME}\",
                  \"deduplication_on_engagement\": \"${DEFECTDOJO_ENGAGEMENT_DEDUPLICATION_ON_ENGAGEMENT}\",
                  \"product\": \"${DEFECTDOJO_PRODUCTID}\",
                  \"source_code_management_uri\": \"${CI_PROJECT_URL}\",
                  \"build_server\": ${DEFECTDOJO_ENGAGEMENT_BUILD_SERVER},
                  \"source_code_management_server\": ${DEFECTDOJO_ENGAGEMENT_SOURCE_CODE_MANAGEMENT_SERVER},
                  \"orchestration_engine\": ${DEFECTDOJO_ENGAGEMENT_ORCHESTRATION_ENGINE}
                }" | jq -r '.id'`
    - echo "DEFECTDOJO_ENGAGEMENTID=${ENGAGEMENTID}" >> defectdojo_zap_proxy.env
  artifacts:
    reports:
      dotenv: defectdojo_zap_proxy.env


defectdojo_publish_gitlab_zap_proxy:
  stage: zap_proxy_publish
  image: alpine
  allow_failure: true
  variables:
    DEFECTDOJO_SCAN_MINIMUM_SEVERITY: "Info"
    DEFECTDOJO_SCAN_ACTIVE: "true"
    DEFECTDOJO_SCAN_VERIFIED: "false"
    DEFECTDOJO_SCAN_CLOSE_OLD_FINDINGS: "true"
    DEFECTDOJO_SCAN_PUSH_TO_JIRA: "false"
    DEFECTDOJO_SCAN_ENVIRONMENT: "Default"
    DEFECTDOJO_SCAN_TEST_TYPE: "ZAP Scan"
    DEFECTDOJO_PRODUCT_TYPE: "gitlab-community"
    DEFECTDOJO_PRODUCT_NAME: "gitlab-defectdojo"
    
  before_script:
    - apk add curl coreutils
    - TODAY=`date +%Y-%m-%d`
  script:
    - |
        curl -X POST "${DEFECTDOJO_URL}/import-scan/" -H  "accept: application/json" -H  "Content-Type: multipart/form-data" -H  "X-CSRFToken: ujMSwx6OazuMFYFeovC7SFWNZ4MeCRS7dqRYTlAHYXBDCGvw5aoC2iWR9m5UCwts" -H "Authorization: Token 0dfc2ae06f011369f4eefbcf3e067efb9627ce09" -F "scan_date=${TODAY}" -F "minimum_severity=Info" -F "active=true" -F "verified=false" -F "scan_type=ZAP Scan" -F "product_name=gitlab-defectdojo" -F "file=@report_zap.xml;type=text/xml" -F "engagement_name=Zap_proxy_#${CI_PIPELINE_ID}" -F "close_old_findings=true" -F "push_to_jira=false"

