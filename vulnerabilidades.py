import requests, json, csv, os
from datetime import datetime
from pathlib import Path
import tempfile
import logging


class Vulnerabilidades:
    """
    Clase encargada de administrar todo lo relacionado con las vulnerabilidades
    encontradas en sonarqube.
    """
    def __init__(self, sonar_url, sonar_token):
        """
            Constructor de la clase.
        """
        logging.basicConfig(format="%(levelname)s: %(asctime)s [+] %(message)s",
                    datefmt="%m/%d/%Y %I:%M:%S %p")

        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        self.sonar_url = sonar_url
        self.sonar_token = sonar_token
        self.lista_vulnerabilidades_json = "ListaDeVulnsSonar_full.json"
        self.lista_vulnerabilidades_csv = "ListaDeVulnsSonar_full.csv"
        self.path_reporte = ""

    def get_data(self):
        """
        Obtenemos la data del archivo csv.
        """

        vulns_path = Path().joinpath(self.path_reporte, self.lista_vulnerabilidades_csv)
# Abrimos el archivo que tiene la data
        with open(vulns_path, "rb") as vulns:
            data = vulns.read()

        return data

    def consulta(self):
        """
        Realizamos la consulta de las vulnerabilidades.
        :param sonar_url url de sonar
        :param sonar_token token de authenticacion para sonar
        """
        self.__crear_carpetas()

        # cantidad de registros por página. Máximo: 500
        registros_por_pagina = 500
        DATA = {
            "p": 1,
            "ps": registros_por_pagina,
            "facets": "severities",
            "types": "VULNERABILITY",
            "severities": ["BLOCKER,CRITICAL"],
            "statuses": [
                "OPEN",
                "REOPENED",
                "TO_REVIEW",
                "IN_REVIEW",
                "REVIEWED",
                "CONFIRMED",
            ],
        }
        requests.packages.urllib3.disable_warnings()
        issues_url = self.sonar_url + "/api/issues/search"

        self.logger.info("Obteniendo los issues")

        raw_response = requests.get(
            url=issues_url,
            params=DATA,
            headers={"Accept": "*/*"},
            auth=(self.sonar_token, ""),
            verify=False,
        )
        response = raw_response.json()

        # cantidad registros
        self.logger.info("Issues obtenidos")
        total = response["total"]
        pages = 1
        output = []

        while total > 0:
            pages += 1
            # saco el page size a la cantidad de registros.
            total -= registros_por_pagina
        # creo un objeto iterable, con un rango entre uno y la cantidad de páginas previamente obtenida.
        pages = range(1, pages)

        for page in pages:
            DATA["p"] = page
            DATA["ps"] = registros_por_pagina

            raw_response= requests.get(
                url=issues_url,
                params=DATA,
                headers={"Accept": "*/*"},
                auth=(self.sonar_token, ""),
                verify=False,
            )
            response = raw_response.json()
            issues = response.get("issues")
            for issue in issues:
                data = {}
                data["key"] = issue["key"]
                data["rule"] = issue["rule"]
                data["severity"] = issue["severity"]
                data["component"] = issue["component"]
                data["project"] = issue["project"]
                data["line"] = issue["line"]
                data["status"] = issue["status"]
                data["message"] = issue["message"]
                data["effort"] = issue.get("effort", "")
                data["creationDate"] = issue["creationDate"]
                data["updateDate"] = issue["updateDate"]
                data["type"] = issue["type"]
                output.append(data)

        self.__guardar_data(output)

    def __guardar_data(self, data):
        """
        Guardamos la data recibida en un archivo .csv.
        :param data
        """
        self.logger.info("Creando Archivos")

        # Creo el archivo para guardar la info
        with open(self.lista_vulnerabilidades_json, "w", encoding="utf-8") as json_file:
            json.dump(data, json_file)

        with open(self.lista_vulnerabilidades_json, "r", encoding="utf-8") as json_file:

            data_result = json.load(json_file)

            employee_data = data_result

            # now we will open a file for writing
            with open(self.lista_vulnerabilidades_csv, "w",
                      newline="", encoding="utf-8") as data_file:

                # create the csv writer object
                csv_writer = csv.writer(data_file)

                # Counter variable used for writing
                # headers to the CSV file
                count = 0

                for emp in employee_data:
                    if count == 0:

                        # Writing headers of CSV file
                        header = emp.keys()
                        csv_writer.writerow(header)
                        count += 1

                    # Writing data of CSV file
                    csv_writer.writerow(emp.values())

        self.logger.info("Listo, archivos creados")

    def __crear_carpetas(self):
        """
        Creamos las carpetas necesarias para guardar los reportes.
        """
        self.logger.info("Creando carpetas")
        tmp_dir = tempfile.mkdtemp()

        ubicacion_absoluta = Path().joinpath(tmp_dir, "sonar_report")

        current_time = datetime.now()
        anio = current_time.strftime("%Y")
        mes = current_time.strftime("%m")
        dia = current_time.strftime("%d")

        reporte_ubicacion = Path().joinpath(anio, mes, dia)

        ruta_final = Path().joinpath(ubicacion_absoluta, reporte_ubicacion)

        os.makedirs(ruta_final, exist_ok=True)

        os.chdir(ruta_final)
        self.logger.info("Ruta donde se guardan los reportes: %s", ruta_final)
        self.path_reporte = ruta_final

